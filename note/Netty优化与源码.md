# Netty优化与源码

书接上文，大部分例子取自于上文中的聊天室功能，[Netty进阶](https://blog.csdn.net/Yellow_Star___/article/details/130826531?spm=1001.2014.3001.5501)

## 1. 优化

### 1.1 扩展序列化算法

序列化，反序列化主要用在消息正文的转换上

* 序列化时，需要将 Java 对象变为要传输的数据（可以是 byte[]，或 json 等，最终都需要变成 byte[]）
* 反序列化时，需要将传入的正文数据还原成 Java 对象，便于处理

目前的代码仅支持 Java 自带的序列化，反序列化机制，核心代码如下

```java
// 反序列化
byte[] body = new byte[bodyLength];
byteByf.readBytes(body);
ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(body));
Message message = (Message) in.readObject();
message.setSequenceId(sequenceId);

// 序列化
ByteArrayOutputStream out = new ByteArrayOutputStream();
new ObjectOutputStream(out).writeObject(message);
byte[] bytes = out.toByteArray();
```

为了支持更多序列化算法，抽象一个 Serializer 接口，内部通过枚举的方式来确定序列化方法

目前支持JDK自带的和JSON

```java
public interface Serializer {
    // 反序列化方法
    <T> T deserialize(Class<T> clazz, byte[] bytes);

    // 序列化方法
    <T> byte[] serialize(T object);

    enum Algorithm implements Serializer{
        Java {
            @Override
            public <T> T deserialize(Class<T> clazz, byte[] bytes) {
                try {
                    ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
                    T t = (T) ois.readObject();
                    return t;
                } catch (IOException | ClassNotFoundException e) {
                    throw new RuntimeException("反序列化失败:{}",e);
                }
            }

            @Override
            public <T> byte[] serialize(T object) {
                try {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(baos);
                    oos.writeObject(object);
                    return baos.toByteArray();
                } catch (IOException e) {
                    throw new RuntimeException("序列化失败:{}",e);
                }
            }
        },
        Json {
            @Override
            public <T> T deserialize(Class<T> clazz, byte[] bytes) {
                return new Gson().fromJson(new String(bytes,StandardCharsets.UTF_8),clazz);
            }

            @Override
            public <T> byte[] serialize(T object) {
                String json = new Gson().toJson(object);
                return json.getBytes(StandardCharsets.UTF_8);
            }
        }
    }
}
```

为了可以更好的设置序列化方式，采取读取配置文件的形式来指定序列化方式

```java
public abstract class Config {
    static Properties properties;
    static {
        try (InputStream in = Config.class.getResourceAsStream("/application.properties")) {
            properties = new Properties();
            properties.load(in);
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }
    }
    public static int getServerPort() {
        String value = properties.getProperty("server.port");
        if(value == null) {
            return 8080;
        } else {
            return Integer.parseInt(value);
        }
    }
    public static Serializer.Algorithm getSerializerAlgorithm() {
        String value = properties.getProperty("serializer.algorithm");
        if(value == null) {
            return Serializer.Algorithm.Java;
        } else {
            return Serializer.Algorithm.valueOf(value);
        }
    }
}
```

配置文件

```properties
serializer.algorithm=Json
```

修改编码器

```java
@Slf4j
@ChannelHandler.Sharable
public class MessageSharableCodec extends MessageToMessageCodec<ByteBuf,Message> {
    @Override
    protected void encode(ChannelHandlerContext ctx, Message msg, List<Object> list) throws Exception {
        ByteBuf out = ByteBufAllocator.DEFAULT.buffer();
        // 1. 4个字节的魔数
        out.writeBytes(new byte[]{1,2,3,4});
        // 2. 1个字节的版本号
        out.writeByte(1);
        // 3. 1个字节的序列化算法,ordinal()会按照枚举顺序返回0,1,2... 0代表java，1代表json
        out.writeByte(Config.getSerializerAlgorithm().ordinal());
        // 4. 1个字节的指令类型
        out.writeByte(msg.getMessageType());
        // 5. 4个字节的请求序号
        out.writeInt(msg.getSequenceId());
        // 6. 补全字节，一般来说都是2的次方
        out.writeByte(0xff);
        //序列化消息内容
        byte[] bytes =Config.getSerializerAlgorithm().serialize(msg);
        // 7. 4个字节的正文长度
        out.writeInt(bytes.length);
        // 8. 正文
        out.writeBytes(bytes);
        //将内容传递到下一个handler
        list.add(out);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        // 1. 魔数
        int magicNumber = in.readInt();
        // 2. 版本号
        byte version = in.readByte();
        // 3. 序列化算法
        byte serializable = in.readByte();
        // 4. 指令类型
        byte messageType = in.readByte();
        // 5. 请求序号
        int sequence = in.readInt();
        // 6. 补全
        in.readByte();
        // 7. 正文长度
        int length = in.readInt();
        // 8. 正文
        byte[] bytes = new byte[length];
        in.readBytes(bytes,0,length);
        // 反序列化
        //根据序列化算法找到对应的Algorithm
        Serializer.Algorithm algorithm = Serializer.Algorithm.values()[serializable];
        //找到实际要转换的类型
        Class<? extends Message> messageClass = Message.getMessageClass(messageType);
        Message message = algorithm.deserialize(messageClass,bytes);
        log.debug("{},{},{},{},{},{}",magicNumber,version,serializable,messageType,sequence,length);
        log.debug("正文:{}",message);
        //将正文交给下一个handler
        out.add(message);
    }
}
```

目前是使用Json格式序列化的，并且同一条指令下，Json格式序列化比JDK序列化的字节更短。

![image-20230523171249637](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305231712743.png)



### 1.2 参数调优

先介绍一下怎么给channel设置参数

```java
//客户端配置的是SockerChannel，可以通过option设置
Bootstrap bootstrap = new Bootstrap().option();

//服务端因为有专门处理连接的ServerSocketchannel，也有处理其他业务的Socketchannel
//option方法是为ServerSocketchannel设置参数
ServerBootstrap option1 = new ServerBootstrap().option();
//childOption方法是为Socketchannel设置参数
ServerBootstrap option2 = new ServerBootstrap().childOption();
```



#### CONNECT_TIMEOUT_MILLIS

顾名思义，这是用来设置连接超时的参数，属于SocketChannel参数，用于在客户端与服务端建立连接时，如果在指定毫秒内无法连接，会抛出timeout异常

**演示**

```java
public static void main(String[] args) {
    NioEventLoopGroup group = new NioEventLoopGroup();
    try {
        ChannelFuture future = new Bootstrap()
                .group(group)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS,300)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new LoggingHandler());
                    }
                })
                .connect(new InetSocketAddress("localhost", 8080));
        future.sync().channel().closeFuture().sync();
    } catch (InterruptedException e) {
        throw new RuntimeException(e);
    }finally {
        group.shutdownGracefully();
    }
}
```

设置超时事件为300毫秒，不启动服务端故意制造超时现象，客户端报错，可以看到连接时长为300毫秒

![image-20230524134945257](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241349292.png)

如果我们设置超时事件为5秒，执行后发现与我们设定的不一样，只经过2秒左右就直接抛异常，并且此异常与上述异常不一样

小黄猜测，这应该是与Netty内部设置的连接的建立事件有关，因为不可能说客户端会一直与服务端进行连接，应该会有一个保护机制，如果多少秒内没有应答，直接抛异常。

![image-20230524135052826](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241350855.png)



**源码解析**

以下是连接超时的源码，Netty中用了很多定时任务，也就是在建立连接时设置一个定时任务，`connectTimeoutMillis`就是`option(ChannelOption.CONNECT_TIMEOUT_MILLIS,300)`中的300，在300毫秒后执行。

![image-20230524140303739](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241403768.png)

定时任务是用另外一个线程执行的，他与主线程之间通过`promise`交换信息，Debug模式调试后，可以看到主线程与定时任务线程共享同一个promise

主线程在`futute.sync()`阻塞，定时任务返回一个异常结果给主线程，主线程抛异常

![image-20230524140906075](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241409112.png)

![image-20230524140932100](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241409141.png)



#### SO_BACKLOG

属于ServerSocketChannel参数，直接将这个参数小黄也讲不清楚，我们先来看一下TCP的三次握手

1. 第一次握手，client 发送 SYN 到 server，状态修改为 SYN_SEND，server 收到，状态改变为 SYN_REVD，并将该请求放入 sync queue 队列
2. 第二次握手，server 回复 SYN + ACK 给 client，client 收到，状态改变为 ESTABLISHED，并发送 ACK 给 server
3. 第三次握手，server 收到 ACK，状态改变为 ESTABLISHED，将该请求从 sync queue 放入 accept queue

![image-20230524142728183](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241427223.png)

* 在 linux 2.2 之前，backlog 大小包括了两个队列的大小，在 2.2 之后，分别用下面两个参数来控制

* sync queue - 半连接队列
  * 大小通过 /proc/sys/net/ipv4/tcp_max_syn_backlog 指定，在 `syncookies` 启用的情况下，逻辑上没有最大值限制，这个设置便被忽略
* accept queue - 全连接队列
  * 其大小通过 /proc/sys/net/core/somaxconn 指定，在使用 listen 函数时，内核会根据传入的 backlog 参数与系统参数，取二者的较小值
  * 如果 accpet queue 队列满了，server 将发送一个拒绝连接的错误信息到 client



**演示**

在Netty中可以通过`option(ChannelOption.SO_BACKLOG, 2)`来设置`accept queue`大小

也就是说`accpet_queue`中只允许暂存两个连接

```java
public static void main(String[] args) {
    NioEventLoopGroup boss = new NioEventLoopGroup();
    NioEventLoopGroup worker = new NioEventLoopGroup();
    try {
        ChannelFuture future = new ServerBootstrap()
                .group(boss, worker)
                .option(ChannelOption.SO_BACKLOG,2)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) {
                        ch.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));
                    }
                })
                .bind(8080);
        future.sync();
        ChannelFuture closeFuture = future.channel().closeFuture();
        closeFuture.sync();
    }catch (Exception e) {
        e.printStackTrace();
    }finally {
        boss.shutdownGracefully();
        worker.shutdownGracefully();
    }
}
```

Netty处理连接的速度是非常快的，可以以debug方式启动服务端，让服务端卡死在处理连接请求，这时候每个连接都是放在`accpet queue`中的

![image-20230524143759895](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241437932.png)

启动客户端，当启动到第三个时，会抛异常，连接被拒绝

![image-20230524143841688](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241438726.png)



**源码**

以下是`accpet queue`默认大小的源码，在windows中默认大小为200，在Linux或macos中为128，在Linux中可以通过`/proc/sys/net/core/somaxconn`来调整

![image-20230524144616574](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241446617.png)





#### ALLOCATOR

属于 SocketChannal 参数，主要作用是来分配ByteBuf

**演示**

在客户端与服务器建立之后，创建一个ByteBuf

```java
ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ByteBuf buffer = ctx.alloc().buffer();
        log.debug("buffer : {}",buffer);
    }
});
```

ByteBuf是池化，使用直接内存的，我这台机子的初始大小为256

![image-20230524150211595](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241502619.png)



**源码**

通过源码可以看到池化、非池化是由`io.netty.allocator.type`指定的，默认情况下安卓系统是非池化，其他系统都是池化

![image-20230524154830143](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241548184.png)

可以通过修改VM参数来指定

![image-20230524154937834](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241549862.png)

是否使用直接内存，是由`io.netty.noPreferDirect`来设置的，这有点绕，他的参数名是不优先使用直接内存，默认false，表示优先使用直接内存

![image-20230524155223674](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241552700.png)

![image-20230524155252781](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241552807.png)

![image-20230524155502757](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241555797.png)



#### RCVBUF_ALLOCATOR

* 属于 SocketChannal 参数
* 控制 netty 接收缓冲区大小
* 负责入站数据的分配，决定入站缓冲区的大小（并可动态调整），统一采用 direct 直接内存，具体池化还是非池化由 allocator 决定

**演示**

简单来说，就是我们一开始接收到数据需要一个区域存放，可以看到默认是池化的使用直接内存

![image-20230524155827796](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305241558827.png)



### 1.3 RPC框架

RPC是指远程过程调用，也就是说两台服务器A，B，一个应用部署在A服务器上，想要调用B服务器上应用提供的函数/方法，由于不在一个内存空间，不能直接调用，需要通过网络来表达调用的语义和传达调用的数据。

**编解码使用聊天室一样的那套**

#### 服务端Handler

服务端处理`RpcRequestMessage`后，调用service方法得到一个结果后包装成`RpcResponseMessage`回给客户端

```java
@ChannelHandler.Sharable
public class RpcRequestMessageHandler extends SimpleChannelInboundHandler<RpcRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequestMessage msg){
        RpcResponseMessage response = new RpcResponseMessage();
        response.setSequenceId(msg.getSequenceId());
        try {
            //利用反射调用
            Object service = ServicesFactory.getService(Class.forName(msg.getInterfaceName()));
            //获取需要调用的方法
            Method method = service.getClass().getMethod(msg.getMethodName(), msg.getParameterTypes());
            //调用该方法
            Object invoke = method.invoke(service,msg.getParameterValue());
            //调用成功
            response.setReturnValue(invoke);
        } catch (Exception e) {
            e.printStackTrace();
            response.setExceptionValue(e);
        }
        ctx.writeAndFlush(response);
    }
}
```

**获取接口的实现类**

```java
public class ServicesFactory {

    static Properties properties;
    static Map<Class<?>, Object> map = new ConcurrentHashMap<>();

    static {
        try (InputStream in = Config.class.getResourceAsStream("/application.properties")) {
            properties = new Properties();
            properties.load(in);
            Set<String> names = properties.stringPropertyNames();
            for (String name : names) {
                if (name.endsWith("Service")) {
                    Class<?> interfaceClass = Class.forName(name);
                    Class<?> instanceClass = Class.forName(properties.getProperty(name));
                    map.put(interfaceClass, instanceClass.newInstance());
                }
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static <T> T getService(Class<T> interfaceClass) {
        return (T) map.get(interfaceClass);
    }
}
```



#### 服务端

```java
public class RpcServer {
    public static void main(String[] args) {
        NioEventLoopGroup boss = new NioEventLoopGroup();
        NioEventLoopGroup worker = new NioEventLoopGroup();
        LoggingHandler LOGGING_HANDLER = new LoggingHandler();
        MessageSharableCodec MESSAGE_SHARABLE_CODEC = new MessageSharableCodec();
        RpcRequestMessageHandler RPC_REQUEST_HANDLER = new RpcRequestMessageHandler();
        try {
            ChannelFuture future = new ServerBootstrap()
                    .group(boss, worker)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new ProtocolFrameDecoder());
                            ch.pipeline().addLast(LOGGING_HANDLER);
                            ch.pipeline().addLast(MESSAGE_SHARABLE_CODEC);
                            ch.pipeline().addLast(RPC_REQUEST_HANDLER);
                        }
                    })
                    .bind(8080).sync();
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }
}
```



#### 客户端Handler（第一版）

```java
@Slf4j
@ChannelHandler.Sharable
public class RpcResponseMessageHandler extends SimpleChannelInboundHandler<RpcResponseMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcResponseMessage msg) throws Exception {
        log.debug("msg : {}",msg);
    }
}
```



#### 客户端（第一版）

```java
@Slf4j
public class RpcClient {
    public static void main(String[] args) {
        NioEventLoopGroup group = new NioEventLoopGroup();
        LoggingHandler LOGGING_HANDLER = new LoggingHandler();
        MessageSharableCodec MESSAGE_SHARABLE_CODEC = new MessageSharableCodec();
        RpcResponseMessageHandler RPC_RESPONSE_HANDLER = new RpcResponseMessageHandler();
        try {
            Channel channel = new Bootstrap()
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new ProtocolFrameDecoder());
                            ch.pipeline().addLast(LOGGING_HANDLER);
                            ch.pipeline().addLast(MESSAGE_SHARABLE_CODEC);
                            ch.pipeline().addLast(RPC_RESPONSE_HANDLER);

                        }
                    })
                    .connect(new InetSocketAddress("localhost",8080)).sync().channel();
            //发送远程调用，异步判断成功或异常
            channel.writeAndFlush(new RpcRequestMessage(
                    1,
                    "com.yellowstar.netty01.service.HelloService",
                    "sayHello",
                    String.class,
                    new Class[]{String.class},
                    new Object[]{"张三"}
            )).addListener(promise -> {
                if (!promise.isSuccess()) {
                    Throwable cause = promise.cause();
                    log.error("error", cause);
                }
            });

            channel.closeFuture().sync();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            group.shutdownGracefully();
        }
    }
}
```



#### Gson序列化问题

启动服务端、客户端，发现程序报错，这是因为我们在序列化时使用的是`Gson`，不支持直接将`Class`对象转换成字符串

![image-20230525142813797](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305251428944.png)

需要让Gson支持序列化Class对象

```java
public interface Serializer {
    // 反序列化方法
    <T> T deserialize(Class<T> clazz, byte[] bytes);

    // 序列化方法
    <T> byte[] serialize(T object);

    enum Algorithm implements Serializer{
        Java {
            @Override
            public <T> T deserialize(Class<T> clazz, byte[] bytes) {
                try {
                    ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
                    T t = (T) ois.readObject();
                    return t;
                } catch (IOException | ClassNotFoundException e) {
                    throw new RuntimeException("反序列化失败:{}",e);
                }
            }

            @Override
            public <T> byte[] serialize(T object) {
                try {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(baos);
                    oos.writeObject(object);
                    return baos.toByteArray();
                } catch (IOException e) {
                    throw new RuntimeException("序列化失败:{}",e);
                }
            }
        },
        Json {
            @Override
            public <T> T deserialize(Class<T> clazz, byte[] bytes) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Class.class, new ClassCodec()).create();
                return gson.fromJson(new String(bytes,StandardCharsets.UTF_8),clazz);
            }

            @Override
            public <T> byte[] serialize(T object) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Class.class, new ClassCodec()).create();
                String json = gson.toJson(object);
                return json.getBytes(StandardCharsets.UTF_8);
            }
        }
    }

    class ClassCodec implements JsonSerializer<Class<?>>, JsonDeserializer<Class<?>>{
        @Override
        public Class<?> deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            try {
                //获取对象全类名
                String className = jsonElement.getAsString();
                return Class.forName(className);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public JsonElement serialize(Class<?> aClass, Type type, JsonSerializationContext jsonSerializationContext) {
            //JsonPrimitive 普通类型
            return new JsonPrimitive(aClass.getName());
        }
    }
}
```



#### 修改客户端

一个正常的客户端，肯定会发很多次请求，所以需要把客户端业务相关代码抽取出来

```java
@Slf4j
public class RpcClientManager {
    private static Channel channel = null;
    private static final Object LOCK = new Object();

    public static void main(String[] args) {
        getChannel().writeAndFlush(new RpcRequestMessage(
                1,
                "com.yellowstar.netty01.service.HelloService",
                "sayHello",
                String.class,
                new Class[]{String.class},
                new Object[]{"张三"}
        ));
    }

    public static Channel getChannel(){
        if (channel != null) {
            return channel;
        }
        synchronized (LOCK) {
            if (channel != null) {
                return channel;
            }
            initChannel();
            return channel;
        }
    }

    private static void initChannel() {
        NioEventLoopGroup group = new NioEventLoopGroup();
        LoggingHandler LOGGING_HANDLER = new LoggingHandler();
        MessageSharableCodec MESSAGE_SHARABLE_CODEC = new MessageSharableCodec();
        RpcResponseMessageHandler RPC_RESPONSE_HANDLER = new RpcResponseMessageHandler();
        try {
            channel = new Bootstrap()
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new ProtocolFrameDecoder());
                            ch.pipeline().addLast(LOGGING_HANDLER);
                            ch.pipeline().addLast(MESSAGE_SHARABLE_CODEC);
                            ch.pipeline().addLast(RPC_RESPONSE_HANDLER);

                        }
                    })
                    .connect(new InetSocketAddress("localhost",8080)).sync().channel();

            //异步处理断开
            channel.closeFuture().addListener(future -> {
                group.shutdownGracefully();
            });
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
```



#### 代理

目前客户端调用方法还是需要采用这种方式

```java
getChannel().writeAndFlush(new RpcRequestMessage(
                1,
                "com.yellowstar.netty01.service.HelloService",
                "sayHello",
                String.class,
                new Class[]{String.class},
                new Object[]{"张三"}
        ));
```

很明显，这种调用方式太不方便了，对于客户端，他们更想通过下面这种方式调用

```java
HelloService service = new HelloService();
service.sayHello("张三");
```

这种方法可以通过代理解决

**修改客户端代码**

```java
@Slf4j
public class RpcClientManager {
    private static Channel channel = null;
    private static final Object LOCK = new Object();

    public static void main(String[] args) {
        HelloService service = getProxyService(HelloService.class);
        service.sayHello("张三");
        service.sayHello("李四");
        service.sayHello("王五");
    }

    //创建代理类，负责组装RpcRequestMessage，发送消息
    public static  <T> T getProxyService(Class<T> classService){
        ClassLoader loader = classService.getClassLoader();
        Class[] interfaces = new Class[]{classService};
        Object instance = Proxy.newProxyInstance(loader,interfaces,((proxy, method, args) -> {
            //组装
            RpcRequestMessage message = new RpcRequestMessage(
                    SequenceIdGenerator.nextId(),
                    classService.getName(),
                    method.getName(),
                    method.getReturnType(),
                    method.getParameterTypes(),
                    args
            );
            //发送消息对象
            getChannel().writeAndFlush(message);
            //返回，暂时不处理
            return null;
        }));
        return (T) instance;
    }

    public static Channel getChannel(){
        if (channel != null) {
            return channel;
        }
        synchronized (LOCK) {
            if (channel != null) {
                return channel;
            }
            initChannel();
            return channel;
        }
    }

    private static void initChannel() {
        NioEventLoopGroup group = new NioEventLoopGroup();
        LoggingHandler LOGGING_HANDLER = new LoggingHandler();
        MessageSharableCodec MESSAGE_SHARABLE_CODEC = new MessageSharableCodec();
        RpcResponseMessageHandler RPC_RESPONSE_HANDLER = new RpcResponseMessageHandler();
        try {
            channel = new Bootstrap()
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new ProtocolFrameDecoder());
                            ch.pipeline().addLast(LOGGING_HANDLER);
                            ch.pipeline().addLast(MESSAGE_SHARABLE_CODEC);
                            ch.pipeline().addLast(RPC_RESPONSE_HANDLER);

                        }
                    })
                    .connect(new InetSocketAddress("localhost",8080)).sync().channel();

            //异步处理断开
            channel.closeFuture().addListener(future -> {
                group.shutdownGracefully();
            });
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
```

`RpcRequestMessage`中的`SequenceId`是每条信息独有的，设计一个简单的自增id

```java
public abstract class SequenceIdGenerator {
    private static final AtomicInteger id = new AtomicInteger();

    public static int nextId() {
        return id.incrementAndGet();
    }
}
```



#### 处理返回

在上一步中，返回信息还没有处理，返回结果是由`RpcResponseMessageHandler`收到的，和调用方法并不在同一个线程中，这里需要用到`promise`，用于线程交互结果

**修改客户端代码**

```java
//创建代理类，负责组装RpcRequestMessage，发送消息
public static  <T> T getProxyService(Class<T> classService){
    ClassLoader loader = classService.getClassLoader();
    Class[] interfaces = new Class[]{classService};
    Object instance = Proxy.newProxyInstance(loader,interfaces,((proxy, method, args) -> {
        int sequenceId = SequenceIdGenerator.nextId();
        //组装
        RpcRequestMessage message = new RpcRequestMessage(
                sequenceId,
                classService.getName(),
                method.getName(),
                method.getReturnType(),
                method.getParameterTypes(),
                args
        );
        //发送消息对象
        ChannelFuture future = getChannel().writeAndFlush(message);
        //准备一个空的promise，用于接收结果
        DefaultPromise<Object> promise = new DefaultPromise<>(getChannel().eventLoop());
        RpcResponseMessageHandler.PROMISES.put(sequenceId,promise);

        promise.await();//阻塞，等待结果
        if (promise.isSuccess()) {
            //调用正常
            return promise.getNow();
        }else {
            //调用失败
            throw new RuntimeException("远程调用失败：" + promise.cause());
        }
    }));
    return (T) instance;
}
```

**修改客户端handler**

```java
@Slf4j
@ChannelHandler.Sharable
public class RpcResponseMessageHandler extends SimpleChannelInboundHandler<RpcResponseMessage> {
    //存储每条信息的promise
    public static final Map<Integer, Promise<Object>> PROMISES = new ConcurrentHashMap<>();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcResponseMessage msg) throws Exception {
        log.debug("msg : {}",msg);
        Promise<Object> promise = PROMISES.remove(msg.getSequenceId()); //使用remove，用过的promise直接删掉，避免空间浪费
        if (promise != null) {
            Object returnValue = msg.getReturnValue();
            Exception exceptionValue = msg.getExceptionValue();
            if (exceptionValue == null) {
                promise.setSuccess(returnValue);
            }else {
                promise.setFailure(exceptionValue);
            }
        }
    }
}
```



#### 异常调用

以上，测试正常的结果都是可以得到结果的，但是当服务端出现异常时，会报以下错误，这是因为错误信息太长，而我们设置的`ProtocolFrameDecoder`最大接收长度是1024

![image-20230525153628497](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305251536554.png)

需要修改服务端handler，对异常信息进行处理，只返回具体信息

```java
@ChannelHandler.Sharable
public class RpcRequestMessageHandler extends SimpleChannelInboundHandler<RpcRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequestMessage msg){
        RpcResponseMessage response = new RpcResponseMessage();
        response.setSequenceId(msg.getSequenceId());
        try {
            //利用反射调用
            Object service = ServicesFactory.getService(Class.forName(msg.getInterfaceName()));
            //获取需要调用的方法
            Method method = service.getClass().getMethod(msg.getMethodName(), msg.getParameterTypes());
            //调用该方法
            Object invoke = method.invoke(service,msg.getParameterValue());
            //调用成功
            response.setReturnValue(invoke);
        } catch (Exception e) {
            e.printStackTrace();
            response.setExceptionValue(new RuntimeException("服务端调用失败： " + e.getCause().getMessage()));
        }
        ctx.writeAndFlush(response);
    }
}
```

![image-20230525154024696](https://gitee.com/yellowstar_459/typora-images/raw/master/images/202305251540731.png)



## 2. 源码解析

