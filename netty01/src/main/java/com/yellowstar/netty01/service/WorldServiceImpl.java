package com.yellowstar.netty01.service;

public class WorldServiceImpl implements WorldService{
    @Override
    public String getName(String name) {
        return "world," + name;
    }
}
