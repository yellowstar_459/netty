package com.yellowstar.netty01.service;

public interface HelloService {
    String sayHello(String name);
}
