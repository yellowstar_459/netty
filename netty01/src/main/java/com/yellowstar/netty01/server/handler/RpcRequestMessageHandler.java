package com.yellowstar.netty01.server.handler;

import com.yellowstar.netty01.message.RpcRequestMessage;
import com.yellowstar.netty01.message.RpcResponseMessage;
import com.yellowstar.netty01.service.HelloService;
import com.yellowstar.netty01.service.HelloServiceImpl;
import com.yellowstar.netty01.service.ServicesFactory;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 服务器接收到远程调用
 */
@ChannelHandler.Sharable
public class RpcRequestMessageHandler extends SimpleChannelInboundHandler<RpcRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequestMessage msg){
        RpcResponseMessage response = new RpcResponseMessage();
        response.setSequenceId(msg.getSequenceId());
        try {
            //利用反射调用
            Object service = ServicesFactory.getService(Class.forName(msg.getInterfaceName()));
            //获取需要调用的方法
            Method method = service.getClass().getMethod(msg.getMethodName(), msg.getParameterTypes());
            //调用该方法
            Object invoke = method.invoke(service,msg.getParameterValue());
            //调用成功
            response.setReturnValue(invoke);
        } catch (Exception e) {
            e.printStackTrace();
            response.setExceptionValue(new RuntimeException("服务端调用失败： " + e.getCause().getMessage()));
        }
        ctx.writeAndFlush(response);
    }

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        RpcRequestMessage message = new RpcRequestMessage(
                1,
            "com.yellowstar.netty01.service.HelloService",
                "sayHello",
                String.class,
                new Class[]{String.class},
                new Object[]{"张三"}
        );

        //利用反射调用
        Object service = ServicesFactory.getService(Class.forName(message.getInterfaceName()));

        //获取需要调用的方法
        Method method = service.getClass().getMethod(message.getMethodName(), message.getParameterTypes());
        //调用该方法
        Object invoke = method.invoke(service,message.getParameterValue());
        System.out.println(invoke);
    }
}
