package com.yellowstar.netty01.server.handler;

import com.yellowstar.netty01.message.GroupChatRequestMessage;
import com.yellowstar.netty01.message.GroupChatResponseMessage;
import com.yellowstar.netty01.server.session.GroupSession;
import com.yellowstar.netty01.server.session.GroupSessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

@ChannelHandler.Sharable
public class GroupChatMessageHandler extends SimpleChannelInboundHandler<GroupChatRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupChatRequestMessage msg) throws Exception {
        GroupSession groupSession = GroupSessionFactory.getGroupSession();
        List<Channel> channelList = groupSession.getMembersChannel(msg.getGroupName());
        for (Channel channel : channelList) {
            channel.writeAndFlush(new GroupChatResponseMessage(msg.getFrom(),msg.getContent()));
        }
    }
}
