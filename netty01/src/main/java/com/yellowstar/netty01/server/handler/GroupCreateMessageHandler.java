package com.yellowstar.netty01.server.handler;

import com.yellowstar.netty01.message.GroupCreateRequestMessage;
import com.yellowstar.netty01.message.GroupCreateResponseMessage;
import com.yellowstar.netty01.server.session.Group;
import com.yellowstar.netty01.server.session.GroupSessionFactory;
import com.yellowstar.netty01.server.session.SessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;
import java.util.Set;

@ChannelHandler.Sharable
public class GroupCreateMessageHandler extends SimpleChannelInboundHandler<GroupCreateRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupCreateRequestMessage msg) throws Exception {
        Group group = GroupSessionFactory.getGroupSession().createGroup(msg.getGroupName(), msg.getMembers());
        if (group == null) {
            List<Channel> channelList = GroupSessionFactory.getGroupSession().getMembersChannel(msg.getGroupName());
            for (Channel channel : channelList) {
                channel.writeAndFlush(new GroupCreateResponseMessage(true,"您已被拉入群聊：" + msg.getGroupName()));
            }
        }else {
            ctx.writeAndFlush(new GroupCreateResponseMessage(false, "组别已存在"));
        }
    }
}
