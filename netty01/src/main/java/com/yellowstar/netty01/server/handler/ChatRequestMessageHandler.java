package com.yellowstar.netty01.server.handler;

import com.yellowstar.netty01.message.ChatRequestMessage;
import com.yellowstar.netty01.message.ChatResponseMessage;
import com.yellowstar.netty01.server.session.SessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

@ChannelHandler.Sharable
public class ChatRequestMessageHandler extends SimpleChannelInboundHandler<ChatRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ChatRequestMessage msg) throws Exception {
        String to = msg.getTo();
        Channel channel = SessionFactory.getSession().getChannel(to);
        if (channel != null) {
            channel.writeAndFlush(new ChatResponseMessage(msg.getFrom(),msg.getContent()));
        }else {
            ctx.writeAndFlush(new ChatResponseMessage(false,"消息发送失败，对方不在线"));
        }
    }
}
