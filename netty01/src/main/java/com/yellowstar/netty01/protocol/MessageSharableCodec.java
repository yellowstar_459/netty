package com.yellowstar.netty01.protocol;

import com.yellowstar.netty01.config.Config;
import com.yellowstar.netty01.message.Message;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@ChannelHandler.Sharable
public class MessageSharableCodec extends MessageToMessageCodec<ByteBuf,Message> {
    @Override
    protected void encode(ChannelHandlerContext ctx, Message msg, List<Object> list) throws Exception {
        ByteBuf out = ByteBufAllocator.DEFAULT.buffer();
        // 1. 4个字节的魔数
        out.writeBytes(new byte[]{1,2,3,4});
        // 2. 1个字节的版本号
        out.writeByte(1);
        // 3. 1个字节的序列化算法,ordinal()会按照枚举顺序返回0,1,2... 0代表java，1代表json
        out.writeByte(Config.getSerializerAlgorithm().ordinal());
        // 4. 1个字节的指令类型
        out.writeByte(msg.getMessageType());
        // 5. 4个字节的请求序号
        out.writeInt(msg.getSequenceId());
        // 6. 补全字节，一般来说都是2的次方
        out.writeByte(0xff);
        //序列化消息内容
        byte[] bytes =Config.getSerializerAlgorithm().serialize(msg);
        // 7. 4个字节的正文长度
        out.writeInt(bytes.length);
        // 8. 正文
        out.writeBytes(bytes);
        //将内容传递到下一个handler
        list.add(out);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        // 1. 魔数
        int magicNumber = in.readInt();
        // 2. 版本号
        byte version = in.readByte();
        // 3. 序列化算法
        byte serializable = in.readByte();
        // 4. 指令类型
        byte messageType = in.readByte();
        // 5. 请求序号
        int sequence = in.readInt();
        // 6. 补全
        in.readByte();
        // 7. 正文长度
        int length = in.readInt();
        // 8. 正文
        byte[] bytes = new byte[length];
        in.readBytes(bytes,0,length);
        // 反序列化
        //根据序列化算法找到对应的Algorithm
        Serializer.Algorithm algorithm = Serializer.Algorithm.values()[serializable];
        //找到实际要转换的类型
        Class<? extends Message> messageClass = Message.getMessageClass(messageType);
        Message message = algorithm.deserialize(messageClass,bytes);
        log.debug("{},{},{},{},{},{}",magicNumber,version,serializable,messageType,sequence,length);
        log.debug("正文:{}",message);
        //将正文交给下一个handler
        out.add(message);
    }
}
