package com.yellowstar.netty01.protocol;

import com.yellowstar.netty01.message.LoginRequestMessage;
import com.yellowstar.netty01.protocol.MessageCodec;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class TestCodec {
    public static void main(String[] args) throws Exception {
        EmbeddedChannel channel = new EmbeddedChannel(
                new LoggingHandler(LogLevel.DEBUG),
                new LengthFieldBasedFrameDecoder(1024,12,4,0,0),
                new MessageSharableCodec()
        );
        LoginRequestMessage message = new LoginRequestMessage("YellowStar", "123456");

        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        new MessageCodec().encode(null,message,buf); //编码

        //模拟半包
        //零拷贝
        ByteBuf b1 = buf.slice(0, 100);
        ByteBuf b2 = buf.slice(100, buf.readableBytes() - 100);
        buf.retain(); //让计数器+1
        channel.writeInbound(b1); // 会执行release()方法，计数器-1
        channel.writeInbound(b2);
    }
}
