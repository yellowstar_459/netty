package com.yellowstar.netty01.client;

import com.yellowstar.netty01.message.RpcRequestMessage;
import com.yellowstar.netty01.protocol.MessageSharableCodec;
import com.yellowstar.netty01.protocol.ProtocolFrameDecoder;
import com.yellowstar.netty01.protocol.SequenceIdGenerator;
import com.yellowstar.netty01.server.handler.RpcResponseMessageHandler;
import com.yellowstar.netty01.service.HelloService;
import com.yellowstar.netty01.service.WorldService;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.DefaultPromise;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;

@Slf4j
public class RpcClientManager {
    private static Channel channel = null;
    private static final Object LOCK = new Object();

    public static void main(String[] args) {
        HelloService service = getProxyService(HelloService.class);
        WorldService worldService = getProxyService(WorldService.class);
        System.out.println(worldService.getName("YellowStar"));
    }

    //创建代理类，负责组装RpcRequestMessage，发送消息
    public static  <T> T getProxyService(Class<T> classService){
        ClassLoader loader = classService.getClassLoader();
        Class[] interfaces = new Class[]{classService};
        Object instance = Proxy.newProxyInstance(loader,interfaces,((proxy, method, args) -> {
            int sequenceId = SequenceIdGenerator.nextId();
            //组装
            RpcRequestMessage message = new RpcRequestMessage(
                    sequenceId,
                    classService.getName(),
                    method.getName(),
                    method.getReturnType(),
                    method.getParameterTypes(),
                    args
            );
            //发送消息对象
            ChannelFuture future = getChannel().writeAndFlush(message);
            //准备一个空的promise，用于接收结果
            DefaultPromise<Object> promise = new DefaultPromise<>(getChannel().eventLoop());
            RpcResponseMessageHandler.PROMISES.put(sequenceId,promise);

            promise.await();//阻塞，等待结果
            if (promise.isSuccess()) {
                //调用正常
                return promise.getNow();
            }else {
                //调用失败
                throw new RuntimeException("远程调用失败：" + promise.cause());
            }
        }));
        return (T) instance;
    }

    public static Channel getChannel(){
        if (channel != null) {
            return channel;
        }
        synchronized (LOCK) {
            if (channel != null) {
                return channel;
            }
            initChannel();
            return channel;
        }
    }

    private static void initChannel() {
        NioEventLoopGroup group = new NioEventLoopGroup();
        LoggingHandler LOGGING_HANDLER = new LoggingHandler();
        MessageSharableCodec MESSAGE_SHARABLE_CODEC = new MessageSharableCodec();
        RpcResponseMessageHandler RPC_RESPONSE_HANDLER = new RpcResponseMessageHandler();
        try {
            channel = new Bootstrap()
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new ProtocolFrameDecoder());
                            ch.pipeline().addLast(LOGGING_HANDLER);
                            ch.pipeline().addLast(MESSAGE_SHARABLE_CODEC);
                            ch.pipeline().addLast(RPC_RESPONSE_HANDLER);

                        }
                    })
                    .connect(new InetSocketAddress("localhost",8080)).sync().channel();

            //异步处理断开
            channel.closeFuture().addListener(future -> {
                group.shutdownGracefully();
            });
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
