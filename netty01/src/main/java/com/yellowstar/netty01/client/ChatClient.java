package com.yellowstar.netty01.client;

import com.yellowstar.netty01.message.*;
import com.yellowstar.netty01.protocol.MessageSharableCodec;
import com.yellowstar.netty01.protocol.ProtocolFrameDecoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class ChatClient {
    public static void main(String[] args) {
        NioEventLoopGroup group = new NioEventLoopGroup();
        MessageSharableCodec MESSAGE_SHARABLE_CODEC = new MessageSharableCodec();
        //唤醒线程
        CountDownLatch countDownLatch = new CountDownLatch(1);
        //记录登录成功状态
        AtomicBoolean LOGIN = new AtomicBoolean(false);
        AtomicBoolean LIFE = new AtomicBoolean(true);
        try {
            ChannelFuture future = new Bootstrap()
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new ProtocolFrameDecoder());
                            ch.pipeline().addLast(MESSAGE_SHARABLE_CODEC);
                            //每隔3秒触发一次IdleState#WRITER_IDLE事件
                            ch.pipeline().addLast(new IdleStateHandler(0,3,0));
                            ch.pipeline().addLast(new ChannelDuplexHandler(){
                                @Override
                                public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
                                    IdleStateEvent event = (IdleStateEvent) evt;
                                    //触发空闲写事件
                                    if (event.state() == IdleState.WRITER_IDLE) {
//                                        log.debug("3s 没有写数据了，发送一个心跳包");
                                        ctx.writeAndFlush(new PingMessage());
                                    }
                                }
                            });
                            ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                                @Override
                                public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                    log.debug("msg : {}",msg);
                                    if (msg instanceof LoginResponseMessage) {
                                        LoginResponseMessage loginResponseMessage = (LoginResponseMessage) msg;
                                        if (loginResponseMessage.isSuccess()) {
                                            LOGIN.set(true);
                                        }
                                        //唤醒线程
                                        countDownLatch.countDown();
                                    }
                                }

                                //连接建立时登录
                                @Override
                                public void channelActive(ChannelHandlerContext ctx) throws Exception {
                                    new Thread(() -> {
                                        //等待用户输入
                                        Scanner scanner = new Scanner(System.in);
                                        System.out.println("请输入用户名:");
                                        String username = scanner.nextLine();
                                        System.out.println("请输入密码:");
                                        String password = scanner.nextLine();
                                        LoginRequestMessage message = new LoginRequestMessage(username, password);
                                        ctx.writeAndFlush(message);
                                        log.info("等待服务器处理。。。");

                                        try {
                                            countDownLatch.await();
                                        } catch (InterruptedException e) {
                                            throw new RuntimeException(e);
                                        }

                                        while (LIFE.get()) {
                                            System.out.println("==================================");
                                            System.out.println("send [username] [content]");
                                            System.out.println("gsend [group name] [content]");
                                            System.out.println("gcreate [group name] [m1,m2,m3...]");
                                            System.out.println("gmembers [group name]");
                                            System.out.println("gjoin [group name]");
                                            System.out.println("gquit [group name]");
                                            System.out.println("quit");
                                            System.out.println("==================================");
                                            String command = scanner.nextLine();
                                            String[] s = command.split(" ");
                                            switch (s[0]) {
                                                case "send" :
                                                    ctx.writeAndFlush(new ChatRequestMessage(username,s[1],s[2]));
                                                    break;
                                                case "gsend" :
                                                    ctx.writeAndFlush(new GroupChatRequestMessage(username,s[1],s[2]));
                                                    break;
                                                case "gcreate" :
                                                    HashSet<String> set = new HashSet<>(Arrays.asList(s[2].split(",")));
                                                    set.add(username);
                                                    ctx.writeAndFlush(new GroupCreateRequestMessage(s[1],set));
                                                    break;
                                                case "gmembers" :
                                                    ctx.writeAndFlush(new GroupMembersRequestMessage(s[1]));
                                                    break;
                                                case "gjoin" :
                                                    ctx.writeAndFlush(new GroupJoinRequestMessage(username,s[1]));
                                                    break;
                                                case "gquit" :
                                                    ctx.writeAndFlush(new GroupQuitRequestMessage(username,s[1]));
                                                    break;
                                                case "quit" :
                                                    ctx.channel().close();
                                                    return;
                                            }
                                        }
                                    },"system in").start();
                                }

                                //连接断开时触发
                                @Override
                                public void channelInactive(ChannelHandlerContext ctx) throws Exception {
                                    log.info("断开连接");
                                    ctx.channel().close();
                                    LIFE.set(false);
                                }
                            });
                        }
                    })
                    .connect(new InetSocketAddress("localhost", 8080)).sync();
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }finally {
            group.shutdownGracefully();
        }
    }
}
