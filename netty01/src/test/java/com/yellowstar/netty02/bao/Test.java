package com.yellowstar.netty02.bao;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.nio.charset.Charset;

public class Test {
    public static void main(String[] args) {
        //相当于服务端
        EmbeddedChannel channel = new EmbeddedChannel(
                new LengthFieldBasedFrameDecoder(1024,0,4,1,4),
                new LoggingHandler(LogLevel.DEBUG)
        );

        //客户端发送数据
        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer();
        convert(buffer,"hello,world");
        convert(buffer,"hi~~~");
        channel.writeInbound(buffer);
    }

    /**
     * 消息处理
     * @param buf
     * @param context 一条消息
     */
    public static void convert(ByteBuf buf,String context) {
        byte[] bytes = context.getBytes(Charset.defaultCharset());
        //int占4个字节，前四个字节表示内容的长度
        buf.writeInt(bytes.length);
        //版本号为1
        buf.writeByte(1);
        //内容
        buf.writeBytes(bytes);
    }
}
