package com.yellowstar.netty02.protocol;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LoggingHandler;

import java.net.InetSocketAddress;

public class TestRedis {
    public static void main(String[] args) {
        NioEventLoopGroup group = new NioEventLoopGroup();
        byte[] line = new byte[]{13,10};
        try {
            ChannelFuture future = new Bootstrap()
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new LoggingHandler());
                            ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                                // 会在连接 channel 建立成功后，会触发 active 事件
                                @Override
                                public void channelActive(ChannelHandlerContext ctx) throws Exception {
                                    ByteBuf buf = ctx.alloc().buffer();
                                    buf.writeBytes("*3".getBytes());
                                    buf.writeBytes(line);
                                    buf.writeBytes("$3".getBytes());
                                    buf.writeBytes(line);
                                    buf.writeBytes("set".getBytes());
                                    buf.writeBytes(line);
                                    buf.writeBytes("$4".getBytes());
                                    buf.writeBytes(line);
                                    buf.writeBytes("name".getBytes());
                                    buf.writeBytes(line);
                                    buf.writeBytes("$10".getBytes());
                                    buf.writeBytes(line);
                                    buf.writeBytes("YellowStar".getBytes());
                                    buf.writeBytes(line);
                                    ctx.writeAndFlush(buf);
                                }
                            });
                        }
                    })
                    .connect(new InetSocketAddress("localhost", 6379)).sync();
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            group.shutdownGracefully();
        }
    }
}
