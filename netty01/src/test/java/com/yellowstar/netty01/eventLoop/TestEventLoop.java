package com.yellowstar.netty01.eventLoop;

import io.netty.channel.DefaultEventLoopGroup;
import io.netty.channel.EventLoop;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class TestEventLoop {
    public static void main(String[] args) {
        NioEventLoopGroup group = new NioEventLoopGroup(2); //可处理 io/普通/定时任务

        group.scheduleAtFixedRate(() -> {
            log.debug("====1");
        },0,1, TimeUnit.SECONDS);

        log.debug("====2");
    }
}
