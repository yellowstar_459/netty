package com.yellowstar.netty01.helloworld;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;

public class NettyServer {
    public static void main(String[] args) {
        // 1. 启动器，负责组装netty组件，启动服务器
        new ServerBootstrap()
                // 2. BossEventLoop, WorkerEventLoop(selector,thread) ,group组
                .group(new NioEventLoopGroup())
                // 3. 选择服务器的ServerSocketChannel实现
                .channel(NioServerSocketChannel.class)
                // 4. boss负责处理连接, worker负责处理读写, 决定了worker能做哪些操作
                .childHandler(
                        // 5. ChannelInitializer 代表和客户端进行数据读写的通道初始化，本身也是一个handler，作用是负责添加别的handler
                        new ChannelInitializer<NioSocketChannel>() {
                            @Override
                            protected void initChannel(NioSocketChannel ch) {
                                // 6. 添加具体的handler
                                ch.pipeline().addLast(new StringDecoder()); // 字符解码，将ByteBuf转换成字符串
                                ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){ // 自定义handler
                                    @Override // 读事件
                                    public void channelRead(ChannelHandlerContext ctx, Object msg) {
                                        System.out.println(msg);
                                    }
                                });
                            }
                        }
                )
                // 7. 绑定端口
                .bind(8080);
    }
}
