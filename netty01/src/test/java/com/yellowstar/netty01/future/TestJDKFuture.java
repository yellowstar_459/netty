package com.yellowstar.netty01.future;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

@Slf4j
public class TestJDKFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //创建线程池
        ExecutorService threadPool = Executors.newFixedThreadPool(2);

        //添加一个异步任务
        Future<Integer> future = threadPool.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                log.debug("正在计算");
                Thread.sleep(1000);
                return 10;
            }
        });

        log.debug("等待中...");
        log.debug("结果为： {}" ,future.get());
    }
}
