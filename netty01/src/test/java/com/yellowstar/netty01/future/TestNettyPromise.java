package com.yellowstar.netty01.future;

import io.netty.channel.EventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.concurrent.DefaultPromise;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

@Slf4j
public class TestNettyPromise {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        NioEventLoopGroup group = new NioEventLoopGroup();
        EventLoop eventLoop = group.next();
        DefaultPromise<Integer> promise = new DefaultPromise<>(eventLoop);

        eventLoop.execute(() -> {
            log.debug("处理中...");
            try {
                Thread.sleep(1000);
                int i = 1/0;
                promise.setSuccess(10);
            } catch (InterruptedException e) {
                promise.setFailure(e);
            }
        });

        log.debug("等待中...");
        log.debug("结果为： {}" ,promise.get());
    }
}
