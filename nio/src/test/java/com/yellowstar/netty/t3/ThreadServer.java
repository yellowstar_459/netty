package com.yellowstar.netty.t3;

import com.yellowstar.netty.t1.ByteBufferUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class ThreadServer {
    public static void main(String[] args) throws IOException {
        //负责处理连接事项的boss线程
        Thread.currentThread().setName("boss");
        Selector boss = Selector.open();
        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.bind(new InetSocketAddress(8080));
        ssc.configureBlocking(false);
        ssc.register(boss,SelectionKey.OP_ACCEPT);
        Worker[] workers = new Worker[2];
        for (int i = 0; i < workers.length; i++) {
            workers[i] = new Worker("worker-" + i);
        }
        //负载均衡计数器
        AtomicInteger atomicInteger = new AtomicInteger();
        while (true) {
            boss.select();
            Iterator<SelectionKey> iter = boss.selectedKeys().iterator();
            while (iter.hasNext()) {
                SelectionKey sscKey = iter.next();
                iter.remove();
                if (sscKey.isAcceptable()) {
                    SocketChannel sc = ssc.accept();
                    sc.configureBlocking(false);
                    log.debug("connected...{}",sc.getRemoteAddress());
                    //将sc注册到worker中的selector中
                    log.debug("before register...{}",sc.getRemoteAddress());
                    workers[atomicInteger.getAndIncrement() % workers.length].init(sc);
                }
            }
        }
    }

    //负责处理读写事项的线程
    static class Worker implements Runnable{
        private Thread thread; // 每一个worker独享一个线程
        private Selector selector; // 每一个worker拥有一个selector
        private String name; // 线程名称
        private volatile boolean start = false; // worker是否第一次运行

        private ConcurrentLinkedQueue<Runnable> queue = new ConcurrentLinkedQueue<>();

        public Worker(String name) {
            this.name = name;
        }

        //初始化worker,start可以保证一个worker中使用同一个thread以及selector
        public void init(SocketChannel sc) throws IOException {
            if (!start) {
                thread = new Thread(this, name);
                selector = Selector.open();
                thread.start();
                start = true;
            }
            queue.add(() -> {
                try {
                    sc.register(selector,SelectionKey.OP_READ);
                    log.debug("after register...{}",sc.getRemoteAddress());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
            //释放阻塞
            selector.wakeup();
        }

        //演示只处理简单的读入请求
        @Override
        public void run() {
            while (true) {
                try {
                    selector.select(); //阻塞
                    Runnable task = queue.poll();
                    if (task != null) {
                        task.run();
                    }
                    Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
                    while (iter.hasNext()) {
                        SelectionKey scKey = iter.next();
                        iter.remove();
                        if (scKey.isReadable()) {
                            SocketChannel channel = (SocketChannel) scKey.channel();
                            ByteBuffer buffer = ByteBuffer.allocate(16);
                            channel.read(buffer);
                            buffer.flip();
                            ByteBufferUtil.debugRead(buffer);
                        }
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
