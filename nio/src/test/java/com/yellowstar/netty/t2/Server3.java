package com.yellowstar.netty.t2;

import com.yellowstar.netty.t1.ByteBufferUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

@Slf4j
public class Server3 {
    public static void main(String[] args) throws IOException {
        //1.创建selector
        Selector selector = Selector.open();
        //2.创建用于监听服务连接的channel
        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.bind(new InetSocketAddress(8080));
        ssc.configureBlocking(false);
        //3.将channel注册到selector中
        SelectionKey sscKey = ssc.register(selector, 0, null);
        log.debug("register key : {}",sscKey);
        //4.设置channel的监听事件类型 OP_ACCEPT 代表连接事件
        sscKey.interestOps(SelectionKey.OP_ACCEPT);
        while (true) {
            // select() 没有事件发生，线程阻塞；有事件发生，线程恢复运行
            selector.select();
            // 处理事件
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                //判断事件类型
                if (key.isAcceptable()) {
                    //如果是接收类型
                    log.debug("key :{}",key);
                    ServerSocketChannel channel = (ServerSocketChannel) key.channel();
                    SocketChannel sc = channel.accept();
                    sc.configureBlocking(false);
                    log.debug("sc {}",sc);
                    //将sc注册到selector中，监听读入事件
                    ByteBuffer buffer = ByteBuffer.allocate(16); //将buffer绑定到SelectionKey
                    SelectionKey scKey = sc.register(selector, SelectionKey.OP_READ,buffer);
                }else if (key.isReadable()) {
                    //如果是读入类型
                    try {
                        SocketChannel channel = (SocketChannel) key.channel();
                        ByteBuffer buffer = (ByteBuffer) key.attachment();
                        int read = channel.read(buffer); //当服务端断开连接时，返回值为-1
                        if (read == -1) {
                            key.cancel();
                        }else {
                            //将buffer根据指定字符分割
                            split(buffer);
                            if (buffer.position() == buffer.limit()) {//代表没读到一个完整信息
                                //创建一个扩容的bytebuffer，复制原来buffer中的信息，并新的buffer存到key中
                                ByteBuffer newBuffer = ByteBuffer.allocate(buffer.capacity() * 2);
                                buffer.flip();
                                newBuffer.put(buffer);
                                key.attach(newBuffer);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        key.cancel();
                    }
                }
                iterator.remove();
            }
        }
    }

    private static void split(ByteBuffer buffer) {
        //将buffer转为读模式
        buffer.flip();
        int limit = buffer.limit();
        for (int i = 0; i < limit; i++) {
            if (buffer.get(i) == '\n') {
                int length = i + 1 - buffer.position();
                //用一个ByteBuffer存起来
                ByteBuffer temp = ByteBuffer.allocate(length);
                buffer.limit(i + 1);
                //从buffer读，写到temp
                temp.put(buffer);
                ByteBufferUtil.debugAll(temp);
                buffer.limit(limit);
            }
        }
        buffer.compact();
    }
}
