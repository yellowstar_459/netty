package com.yellowstar.netty.t2;

import com.yellowstar.netty.t1.ByteBufferUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

@Slf4j
public class Server2 {
    public static void main(String[] args) throws IOException {
        // 1.创建服务器
        ServerSocketChannel ssc = ServerSocketChannel.open();
        // 2.绑定监听端口
        ssc.bind(new InetSocketAddress(8080));
        // 设置服务器为非阻塞状态
        ssc.configureBlocking(false);
        // 3.建立连接集合
        ArrayList<SocketChannel> channels = new ArrayList<>();
        while (true) {
            // 4.accept 建立与客户端连接， SocketChannel 用来与客户端之间通信
            SocketChannel socketChannel = ssc.accept(); //如果没有连接，返回null
            if (socketChannel != null) {
                // 设置通道为非阻塞状态
                socketChannel.configureBlocking(false);
                log.debug("connected!");
                channels.add(socketChannel);
            }
            for (SocketChannel channel : channels) {
                // 5.开辟一个缓存区
                ByteBuffer buffer = ByteBuffer.allocate(16);
                int read = channel.read(buffer);// 如果没有读到字节返回0
                if (read > 0) {
                    // 6.切换为读模式
                    buffer.flip();
                    ByteBufferUtil.debugRead(buffer);
                    // 7.切换为写模式
                    buffer.clear();
                    log.debug("after read...{}", channel);
                }
            }
        }
    }
}
