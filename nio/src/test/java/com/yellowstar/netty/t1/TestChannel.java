package com.yellowstar.netty.t1;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

@SpringBootTest
public class TestChannel {
    @Test
    public void transferTo(){
        try (
                FileChannel from = new FileInputStream("demo.txt").getChannel();
                FileChannel to = new FileOutputStream("to.txt").getChannel()
        ) {
            long size = from.size();
            //left表示还有多少文件未读取
            for (long left = size;left > 0;) {
                left -= from.transferTo(0, size, to);
            }
        } catch (IOException e) {
        }
    }
}
