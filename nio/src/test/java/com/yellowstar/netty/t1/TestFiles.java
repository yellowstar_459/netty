package com.yellowstar.netty.t1;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootTest
public class TestFiles {
    @Test
    public void test1(){
        Path path = Paths.get("demo11.txt");
        boolean exists = Files.exists(path);
        System.out.println(exists);
    }

    @Test
    public void test2() throws IOException {
        Path path = Paths.get("hello1/d1");
        Files.createDirectory(path);
    }

    @Test
    public void test3() throws IOException {
        Path path = Paths.get("hello1/d1");
        Files.createDirectories(path);
    }

    @Test
    public void test4() throws IOException {
        Path source = Paths.get("hello/source.txt");
        Path target = Paths.get("hello/target.txt");
        Files.copy(source,target, StandardCopyOption.REPLACE_EXISTING);
    }

    @Test
    public void test5() throws IOException {
        Path source = Paths.get("hello/source.txt");
        Path target = Paths.get("hello1/source.txt");
        Files.move(source,target, StandardCopyOption.ATOMIC_MOVE);
    }

    @Test
    public void test6() throws IOException {
        Path path = Paths.get("hello1/source.txt");
        Files.delete(path);
    }

    @Test
    public void test7() throws IOException {
        Path path = Paths.get("E:\\hkx\\apache-maven-3.9.1");
        AtomicInteger dirCount = new AtomicInteger();
        AtomicInteger fileCount = new AtomicInteger();
        Files.walkFileTree(path,new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                System.out.println("====>  " + dir);
                dirCount.incrementAndGet();
                return super.preVisitDirectory(dir, attrs);
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                System.out.println(file);
                fileCount.incrementAndGet();
                return super.visitFile(file, attrs);
            }
        });
        System.out.println("文件夹数量：" + dirCount);
        System.out.println("文件数量：" + fileCount);
    }

    @Test
    public void test8() throws IOException {
        Path path = Paths.get("E:\\hkx\\apache-maven-3.9.1");
        AtomicInteger jarCount = new AtomicInteger();
        Files.walkFileTree(path,new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.toString().endsWith(".jar")) {
                    System.out.println(file);
                    jarCount.incrementAndGet();
                }
                return super.visitFile(file, attrs);
            }
        });
        System.out.println("jar数量：" + jarCount);
    }

    @Test
    public void test9() throws IOException {
        Path path = Paths.get("E:\\nacos");
        Files.walkFileTree(path,new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return super.postVisitDirectory(dir, exc);
            }
        });
    }

    @Test
    public void  test10() throws IOException {
        String source = "E:\\nacos";
        String target = "E:\\nacosaaa";
        Files.walk(Paths.get(source)).forEach(path -> {
            try {
                String file = path.toString().replace(source,target);
                //如果是目录则创建
                if (Files.isDirectory(path)) {
                    Files.createDirectory(Paths.get(file));
                }else if (Files.isRegularFile(path)){ //如果是普通文件
                    Files.copy(path,Paths.get(file));
                }
            }catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
