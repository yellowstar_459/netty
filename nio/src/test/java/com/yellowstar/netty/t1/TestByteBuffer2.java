package com.yellowstar.netty.t1;

import lombok.extern.slf4j.Slf4j;

import java.nio.ByteBuffer;

@Slf4j
public class TestByteBuffer2 {
    public static void main(String[] args) {
        ByteBuffer source = ByteBuffer.allocate(32);
        source.put("Hello,world\nI'm zhangsan\nHo".getBytes());
        split(source);

        source.put("w are you?\nhaha!\n".getBytes());
        split(source);
    }

    private static void split(ByteBuffer buffer) {
        //将buffer转为读模式
        buffer.flip();
        int limit = buffer.limit();
        for (int i = 0; i < limit; i++) {
            if (buffer.get(i) == '\n') {
                int length = i + 1 - buffer.position();
                //用一个ByteBuffer存起来
                ByteBuffer temp = ByteBuffer.allocate(length);
                buffer.limit(i + 1);
                //从buffer读，写到temp
                temp.put(buffer);
                ByteBufferUtil.debugAll(temp);
                buffer.limit(limit);
            }
        }
        buffer.compact();
    }
}
