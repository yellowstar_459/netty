package com.yellowstar.netty.t1;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

@Slf4j
@SpringBootTest
public class TestByteBuffer {
    @Test
    public void byteBuffer(){
        /**
         * 获取FileChannel(两种方式)
         * 1. 输入输出流
         * 2. RandomAccessFile
         */
        try(FileChannel channel = new FileInputStream("test.txt").getChannel()) { //channel实现了Closeable接口，可以结束时自动释放资源
            //创建一个缓冲区
            ByteBuffer buffer = ByteBuffer.allocate(10);
            while (true) {
                //从channel中读取数据，写入buffer
                int len = channel.read(buffer);
                log.debug("当前读取字节数:{}" , len);
                if (len == -1) { //len为-1时代表数据已经读完
                    break;
                }
                //打印buffer中的内容
                buffer.flip(); //将buffer切换为读模式
                while (buffer.hasRemaining()) { //只要buffer中还有数据就一直读
                    byte b = buffer.get();
                    log.debug("实际字节:{}", (char) b);
                }
                buffer.clear(); //将buffer切换为写模式
            }
        }catch (IOException e) {
        }
    }

    @Test
    public void byteBuffer1() {
        ByteBuffer buffer = ByteBuffer.allocate(16);
        buffer.put("hello".getBytes());
        ByteBufferUtil.debugAll(buffer);
    }

    @Test
    public void byteBuffer2() {
        ByteBuffer buffer = StandardCharsets.UTF_8.encode("hello");
        ByteBufferUtil.debugAll(buffer);
    }

    @Test
    public void byteBuffer3() {
        ByteBuffer buffer = ByteBuffer.wrap("hello".getBytes());
        ByteBufferUtil.debugAll(buffer);
        String s = StandardCharsets.UTF_8.decode(buffer).toString();
        System.out.println(s);
    }

    @Test
    public void scatteringRead(){
        //使用RandomAccessFile创建channel
        try (FileChannel channel = new RandomAccessFile("demo.txt", "r").getChannel()) { // r表示只读
            ByteBuffer b1 = ByteBuffer.allocate(5);
            ByteBuffer b2 = ByteBuffer.allocate(3);
            ByteBuffer b3 = ByteBuffer.allocate(5);
            channel.read(new ByteBuffer[]{b1,b2,b3});
            ByteBufferUtil.debugAll(b1);
            ByteBufferUtil.debugAll(b2);
            ByteBufferUtil.debugAll(b3);
        } catch (IOException e) {
        }
    }

    @Test
    public void gatheringWrites(){
        try (FileChannel channel = new RandomAccessFile("demo2.txt", "rw").getChannel()) { // rw代表读写
            ByteBuffer b1 = StandardCharsets.UTF_8.encode("yellow");
            ByteBuffer b2 = StandardCharsets.UTF_8.encode("star");
            ByteBuffer b3 = StandardCharsets.UTF_8.encode("459");
            channel.write(new ByteBuffer[]{b1,b2,b3});
        } catch (IOException e) {
        }
    }
}
